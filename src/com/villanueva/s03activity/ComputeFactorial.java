package com.villanueva.s03activity;

import java.util.Scanner;

public class ComputeFactorial {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int answer = 1;

        System.out.println("Enter a number to be Factorial: ");

        try{
            int num = Integer.parseInt(in.nextLine());
            //validation
            if(num <= 0){
                throw new NumberFormatException();
            }
            // factorial of a number process
            for(int counter = num; counter >= 1; counter--){
                answer *= counter;
            }

            System.out.println("The Factorial of "+num+" is "+answer);


        }catch(NumberFormatException e){
            System.out.println("Entered value shouldn't be less than or equal to zero[0] or value should be an Integer.");
        }


        // exception handler for getting zero and none numeric value;

    }
}
